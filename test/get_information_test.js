var assert = require("assert");
var app = require("../app");
var request = require("supertest");

//get information about hardware
describe('GET /fuse/hw/', function () {
	it('/fuse/hw/', function (done) {	
		var result = '{"err":0,"size":2,"list":[{"GBNK":"566111.001","name":"БИАБ-1000","last_mod":"02"},{"GBNK":"566111.002","name":"БИАБ-1001","last_mod":"02"}]}';
		
		request(app)
		.get('/fuse/hw')
		.expect(result, done);
    });
});

//get information about modification
describe('GET /fuse/hw/info', function () {
	it('/fuse/hw/', function (done) {	
		result = '{"err":0,"size":1,"list":[{"GBNK":"566111.002","name":"БИАБ-1001","mod":"02","locked":null,"year":2011,"comment":"Зав.№10-20 (Щ02 – Modbus)","debug":"2.3","production":"2.3.4"}]}';
		
		request(app)
		.get('/fuse/hw/info?gbnk=566111.002&number=2')
		.expect(result, done);
    });
	it('/fuse/hw/', function (done) {	
		result = '{"err":0,"size":0,"list":[]}';
		
		request(app)
		.get('/fuse/hw/info?gbnk=566111.002&number=101')
		.expect(result, done);
    });
});

//get information about drivers
describe('GET /fuse/fw/info', function() {
	it('/fuse/fw/', function(done) {
		result = '{"err":0,"size":1,"list":[{"version":"1.2.3","comment":"comment3"}]}';
		
		request(app)
		.get('/fuse/fw/info?gbnk=566111.001&number=1')
		.expect(result, done);
	});
	it('/fuse/fw/', function(done){
		result = '{"err":0,"size":1,"list":[{"GBNK":"566111.001","name":"БИАБ-1000","mod":"01","locked":null,"year":2009,"comment":"Зав.№1-9","debug":"1.2","production":"1.2.3"}]}';
		
		request(app)
		.get('/fuse/hw/info?gbnk=566111.001&number=1&debug=true')
		.expect(result, done);
	});

});