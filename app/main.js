var express = require('express'); //http-server framework
var app     = express();
var config  = require('../config'); //config file
var mysql   = require('mysql'); //database mysql framework
var bodyParser = require('body-parser');//this framework for get data from client and parse this as json
var fs = require('fs');//framework for work with filesystem
var multer = require('multer');//framework for download file from client and save on server
var mkdirp = require('mkdirp');//framework for create directory

//export server
module.exports = app;

//use parser json format to handle client change information request
app.use(bodyParser.json());

//pool connenctoin
connectionpool = mysql.createPool({
    host     :  config.mysql.host,
    user     :  config.mysql.user,
    password :  config.mysql.password,
    database :  config.mysql.database
});

// flag for downloading file
var done = false; 

//multer framework for save file. Overload 'rename', 'changeDest', 'onFileUploadComplete' method.
app.use(multer({ dest: './fuse/',
    rename: function (fieldname, filename, req, res) {
        return  "update";
    },
    changeDest: function (dest, req, res){
        //set directiry where saving file. Actually, i don't know how it works, but it does.
        //TODO add to directory folder 'version' , example: '1.2.3-03042015'. Full dir '566111.001/1/1.2.3-03042015/update.zip'
        dest =  dest +  req.body.gbnk + '/' + req.body.mod // + version (make by function makeNameFile);
        var stat = null;


        try {
            stat = fs.accessSync(dest);
        } catch(err) {
            mkdirp(dest);
        }

        if (stat && !stat.isDirectory()) {
            throw new Error('Directory cannot be created because an inode of a different type exists at "' + dest + '"');
        }

    return dest;   
    },
    onFileUploadComplete: function (file) {
        console.log(file.fieldname + ' uploaded to  ' + file.path);
        done=true;
    }
}));

//if user response will return info from db, we use this callback
function sendGetInfoResponse(res, data){
    res.send({
        err:    0,
        size: data.length,
        list:   data,
    });
}

//if user response will change some info, we use this callback. 
function sendChangeInfoResponse(res, data){
    res.send({
        err:    0,
    });
}

//if we have error when try connection to db
function onErrorConnectionToDbSend(res, err){
    res.statusCode = 503;
    res.send({
        result: 'error',
        err:    err.code
    });
}

//if we have error when try request to db (for example syntax error)
//TODO handle each error and send to user info about error
function onErrorRequsetToDbSend(res, err){
    res.statusCode = 500;
    res.send({
        result: 'error',
        err: err.code
    });
}

function doRequest (query, res, callback){
    //try get connection from connection pool
    connectionpool.getConnection(function(err, connection) {
        if (err) 
            //TODO if we can't use connection, we should catch error and say about it to client
            onErrorConnectionToDbSend(res, err);
        else {
            //if we can use connection, make request to db and send data in response.
            connection.query(query,  function(err, rows, fields) {
            if (err) 
                //TODO if we can't make request to db, we should catch error and say about it to client
                onErrorRequsetToDbSend(res, err);
            else 
                //if request successful, use collback
                callback(res, rows);
                
            });
        //return connection to pool
        connection.release();
        };

    });
}

//when file download we should rename them. 
function makeNameFile(x,y,z,type,rank){
    if (type = 'debug'){
        x = 0;
        switch (rank) {
            case 'minor':
                y++;
                break;
            case 'patch':
                z++;
                break;
            default:
            return 'error';
        }
    } else {
        if (x == 0) {
            x++;
            y = 0;
            z = 0;
        } else {
        switch (rank){
            case 'major':
                x++; y=0; z=0;
                break;
            case 'minor':
                y++; z=0;
                break;
            case 'patch':
                z++;
                break;
            default:
                return 'error';
        }   
    } 
    }
    var now = new Date();
    var name = x +'.' + y + '.' + z + '.' + now.getFullYear() +'-'+ (now.getMonth() + 1) +'-'+ now.getDate();
    return name;
}

//get page for send driver to server.
app.get('/', function(req,res){
    res.sendfile('views/index.html')
});

//get information about hardware's
app.get('/fuse/hw', function(req, res){ 
    doRequest('SELECT * FROM hardware_type_info', res, sendGetInfoResponse);
});

//get information about modification's
//GBNK | number
app.get('/fuse/hw/info', function(req, res){
    var select = 'SELECT `GBNK`, `name`, `number` `mod`, `locked`, `year`, `comment`, MAX(`debug`) debug, MAX(`production`) production ';
    var from   = 'FROM hardware_modification_info ';
    var where  = 'WHERE GBNK = ' + req.query.gbnk + ' AND number = ' + req.query.number + ' ';
    var group  = 'GROUP BY number';

    var query = select + from + where + group;

    doRequest(query, res, sendGetInfoResponse);
});

//get information about modification
//debug (true/false) | GBNK | number
app.get('/fuse/fw/info', function(req, res){
    //we can send information for production or debug drivers. So, we choose what client want
    var table;
    if (req.query.debug != 'true') {
        table = 'driver_production';
    } else {
        table = 'driver_debug';
    }

    var select = 'SELECT `full_name` `version`, `comment` ';
    var from   = 'FROM ' + table + ' WHERE hardware_modification_id = (SELECT hardware_modification_id FROM hardware_modification_info ' 
    var where  = 'WHERE GBNK = ' + req.query.gbnk + ' AND number = ' + req.query.number +' )';

    var query = select + from + where;

    doRequest(query, res, sendGetInfoResponse);
});

//send file to client
//GBNK | number | version
app.get('/fuse/fw', function(req, res){
    res.download('./fuse/' +  query.gbnk + '/' + query.number + '/' + query.version + '/update.zip');
});

//add hardware
//GBNK | name
app.post('/fuse/hw/add', function(req, res){
    var insert = 'INSERT INTO drivers.hardware_type ( GBNK, name) ';
    var values = 'VALUES ( \'' +req.body.gbnk+ '\', \'' + req.body.name + '\' ); '; 
    var query = insert + values; 

    doRequest(query, res, sendChangeInfoResponse);
});

//add modification
//GBNK | mod | lock | year | comment
app.post('/fuse/hw/add_mod', function(req, res){
    var hardware_type_id = ' (SELECT id FROM hardware_type WHERE GBNK = \'' + req.body.gbnk + '\' )'; 
    var values =  hardware_type_id +  ', \'' + req.body.mod + '\', \'' + req.body.lock + '\', \'' + req.body.year + '\', \'' + req.body.comment +'\'';
    var query = 'INSERT INTO drivers.hardware_modification (hardware_type_id, number, locked, year, comment) VALUES ( ' + values + ' );';
    
    doRequest(query, res, sendChangeInfoResponse);
});

//edit modifiction
//TODO realize
app.post('/fuse/hw/edit_mod', function(req, res){
    res.send('in work');
});

//download file to server
//TODO describe information
app.post('/fuse/fw/add', function(req, res){
    if(done==true){
    res.end("File uploaded.");
    }
});

//delete firmware driver from server
//TODO describe information
app.post('/fuse/fw/del', function(req, res){

    var query = 'hardware_modification_id = (SELECT id FROM hardware_modification WHERE ' + 
                'hardware_type_id = ( SELECT id FROM hardware_type WHERE gbnk = \'' + req.body.gbnk + 
                '\' ) ' + ' AND number = \'' + req.body.mod + '\' ) AND full_name = \'' + req.body.version + '\'';
    if (req.body.debug != 'true') {
        doRequest('DELETE FROM driver_production WHERE ' + query, res);
    } else {
        doRequest('DELETE FROM driver_debug WHERE ' + query, res);
    }
});
