-- -----------------------------------------------------
-- Data for table `drivers`.`hardware_type`
-- -----------------------------------------------------
START TRANSACTION;
USE `drivers`;
INSERT INTO `drivers`.`hardware_type` ( `GBNK`, `name`) VALUES ( '566111.001', 'БИАБ-1000');
INSERT INTO `drivers`.`hardware_type` ( `GBNK`, `name`) VALUES ( '566111.002', 'БИАБ-1001');

COMMIT;


-- -----------------------------------------------------
-- Data for table `drivers`.`hardware_modification`
-- -----------------------------------------------------
START TRANSACTION;
USE `drivers`;
INSERT INTO `drivers`.`hardware_modification` ( `hardware_type_id`, `number`, `year`, `comment`) VALUES ( 1, 1, 2009, 'Зав.№1-9');
INSERT INTO `drivers`.`hardware_modification` ( `hardware_type_id`, `number`, `year`, `comment`) VALUES ( 1, 2, 2011, 'Зав.№10-20 (Щ02 – Modbus)');
INSERT INTO `drivers`.`hardware_modification` ( `hardware_type_id`, `number`, `year`, `comment`) VALUES ( 2, 1, 2009, 'Зав.№1-9');
INSERT INTO `drivers`.`hardware_modification` ( `hardware_type_id`, `number`, `year`, `comment`) VALUES ( 2, 2, 2011, 'Зав.№10-20 (Щ02 – Modbus)');

COMMIT;


-- -----------------------------------------------------
-- Data for table `drivers`.`driver_production`
-- -----------------------------------------------------
START TRANSACTION;
USE `drivers`;
INSERT INTO `drivers`.`driver_production` (`major`, `minor`, `patch`, `full_name`, `date`, `path`, `hardware_modification_id`, `comment`) VALUES ( 1, 2, 3, '1.2.3', '12.12.2010', '', 1, 'comment3');
INSERT INTO `drivers`.`driver_production` (`major`, `minor`, `patch`, `full_name`, `date`, `path`, `hardware_modification_id`, `comment`) VALUES ( 2, 3, 4, '2.3.4', '23.11.2014', '', 2, 'comment4');
INSERT INTO `drivers`.`driver_production` (`major`, `minor`, `patch`, `full_name`, `date`, `path`, `hardware_modification_id`, `comment`) VALUES ( 1, 2, 3, '1.2.3', '12.12.2010', '', 3, 'comment3');
INSERT INTO `drivers`.`driver_production` (`major`, `minor`, `patch`, `full_name`, `date`, `path`, `hardware_modification_id`, `comment`) VALUES ( 2, 3, 4, '2.3.4', '23.11.2014', '', 4, 'comment4');

COMMIT;

-- -----------------------------------------------------
-- Data for table `drivers`.`driver_debug`
-- -----------------------------------------------------
START TRANSACTION;
USE `drivers`;
INSERT INTO `drivers`.`driver_debug` (`minor`, `patch`, `full_name`, `date`, `path`, `hardware_modification_id`, `comment`) VALUES ( 1, 2, '1.2', '13.11.2009', '', 1, 'comment1');
INSERT INTO `drivers`.`driver_debug` (`minor`, `patch`, `full_name`, `date`, `path`, `hardware_modification_id`, `comment`) VALUES ( 2, 3, '2.3', '14.12.2013', '', 2, 'comment2');
INSERT INTO `drivers`.`driver_debug` (`minor`, `patch`, `full_name`, `date`, `path`, `hardware_modification_id`, `comment`) VALUES ( 1, 2, '1.2', '13.11.2009', '', 3, 'comment1');
INSERT INTO `drivers`.`driver_debug` (`minor`, `patch`, `full_name`, `date`, `path`, `hardware_modification_id`, `comment`) VALUES ( 2, 3, '2.3', '14.12.2013', '', 4, 'comment2');

COMMIT;

