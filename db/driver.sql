SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `drivers` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `drivers` ;

-- -----------------------------------------------------
-- Table `drivers`.`hardware_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `drivers`.`hardware_type` ;

CREATE TABLE IF NOT EXISTS `drivers`.`hardware_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `GBNK` CHAR(10) NOT NULL,
  `name` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `GBNK_UNIQUE` (`GBNK` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `drivers`.`hardware_modification`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `drivers`.`hardware_modification` ;

CREATE TABLE IF NOT EXISTS `drivers`.`hardware_modification` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `hardware_type_id` INT NOT NULL,
  `number` TINYINT (2) ZEROFILL NOT NULL ,
  `year` YEAR NULL,
  `locked` TINYINT (1) NULL,
  `comment` TEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_hardware_modification_hardware_type1_idx` (`hardware_type_id` ASC),
  CONSTRAINT `fk_hardware_modification_hardware_type1`
    FOREIGN KEY (`hardware_type_id`)
    REFERENCES `drivers`.`hardware_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `drivers`.`driver_production`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `drivers`.`driver_production` ;

CREATE TABLE IF NOT EXISTS `drivers`.`driver_production` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `hardware_modification_id` INT NOT NULL,
  `major` TINYINT(3) UNSIGNED NOT NULL,
  `minor` TINYINT(3) UNSIGNED NOT NULL,
  `patch` TINYINT(3) UNSIGNED NOT NULL,
  `full_name` VARCHAR(20) NOT NULL,
  `date` DATE NOT NULL,
  `path` VARCHAR(255) NOT NULL,
  `comment` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_driver_hardware_modification_idx` (`hardware_modification_id` ASC),
  CONSTRAINT `fk_driver_hardware_modification`
    FOREIGN KEY (`hardware_modification_id`)
    REFERENCES `drivers`.`hardware_modification` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `drivers`.`driver_debug`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `drivers`.`driver_debug` ;

CREATE TABLE IF NOT EXISTS `drivers`.`driver_debug` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `hardware_modification_id` INT NOT NULL,
  `minor` TINYINT(3) UNSIGNED NOT NULL,
  `patch` TINYINT(3) UNSIGNED NOT NULL,
  `full_name` VARCHAR(20) NOT NULL,
  `date` DATE NOT NULL,
  `path` VARCHAR(255) NOT NULL,
  `comment` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_driver_debug_hardware_modification1_idx` (`hardware_modification_id` ASC),
  CONSTRAINT `fk_driver_debug_hardware_modification1`
    FOREIGN KEY (`hardware_modification_id`)
    REFERENCES `drivers`.`hardware_modification` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- -----------------------------------------------------
-- View `drivers`.`hardware_type_info`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `drivers`.`hardware_type_info` ;

CREATE VIEW `drivers`.`hardware_type_info`
  AS SELECT `hardware_type`.`GBNK`, `hardware_type`.`name`, MAX(`hardware_modification`.`number`) `last_mod`
  FROM `hardware_type`
  JOIN `hardware_modification`
  ON `hardware_type`.`id` = `hardware_modification`.`hardware_type_id`
  GROUP BY `hardware_modification`.`hardware_type_id`;


  -- -----------------------------------------------------
-- View `drivers`.`hardware_type_info`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `drivers`.`hardware_modification_info` ;

CREATE VIEW `drivers`.`hardware_modification_info`
  AS SELECT `hardware_type`.`GBNK`, `hardware_type`.`name`, `hardware_modification`.`number`, `hardware_modification`.`id` `hardware_modification_id`, `hardware_modification`.`locked`,
  `hardware_modification`.`year`, `hardware_modification`.`comment`, `driver_production`.`full_name` `production`, `driver_debug`.`full_name` `debug`
  FROM `hardware_modification`
  JOIN `hardware_type`
  ON `hardware_modification`.`hardware_type_id` = `hardware_type`.`id`
  JOIN `driver_production`
  ON `driver_production`.`hardware_modification_id` = `hardware_modification`.`id`
   JOIN `driver_debug`
  ON `driver_debug`.`hardware_modification_id` = `hardware_modification`.`id`;


